#!/usr/bin/python
from time import time
import curses
from Screen import Monitor
from Router import Router
from Address import *;

def readStatic(input):
    dataDict = {};
    category = None;
    for line in input:
        if line[0] != "[":
            category = line.rstrip();
            dataDict[category] = [];
        else:
            dline = [];
            value = line.rstrip();
            value = value[1:len(value) - 1];
            data = value.split(",");
            for x in range(0, len(data)):
                data[x] = data[x].strip();
                data[x] = data[x][1:len(data[x]) - 1];
                data[x] = str(data[x]);
                dline.append(data[x]);
            dataDict[category].append(dline);

    return dataDict;

def paintElement(y, x, element, screen):
    for v in range(0, len(element.arr)):
        hy = y + v;
        screen.addnstr(hy, x, element.arr[v], element.width);

    return (y + element.height, x + element.width);

def updateScreen(stdscr, walks):
    x = 0;
    y = 0;
    elements = [];
    for entry in walks:
        elements.append(getRouterElement(entry));

    paintElement(0, 0, elements[0], stdscr);

    return;

def test():
    buell = Router(__BUELL__, "buell", "2c");
    #harley = Router(__HARLEY__, "harley", "2c");
    #davidson = Router(__DAVIDSON__, "davidson", "1");
    
    for router in [buell]:#, harley, davidson]:
        router.update();
        print router.routes;
        print router.interfaces;
        print router.addresses;
        print router.errors;

def monitor(stdscr):
    nextRefresh = time();

    screen = Monitor(stdscr);  # Associates the screen object with the monitor

    while(1):
        screen.refresh();
        if(time() >= nextRefresh):
            walks = []
            nextRefresh = time() + 15;

            #updateScreen(stdscr, walks);

        c = stdscr.getch()
        if c == ord('1'):
            x = 0;  # PrintDocument()
        elif c == ord('2'):
            x = 1;
        elif c == ord('3'):
            x = 2;
        elif c == ord('4'):
            x = 3
        elif c == ord('q'):
            return;


def main():
    global __VERBOSE__;
    __VERBOSE__ = True;
    '''curses.wrapper(monitor);'''
    test();
    return 0;

__WIDTH__ = 80;
__HEIGHT__ = 24;
__VERBOSE__ = False;
__CWIN__ = None;
__WAITTIME__ = 5;
__BUELL__ = "132.235.201.40";
__HARLEY__ = "132.235.201.50";
__DAVIDSON__ = "132.235.201.60";
if(__name__ == "__main__"):
    main();

