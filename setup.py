__author__="jswaro"
__date__ ="$Mar 8, 2012 4:56:51 PM$"

from distutils.core import setup
#from setuptools import setup,find_packages

setup (
  name = 'pyMonitor',
  version = '0.1',
#  packages = find_packages(),

  # Declare your packages' dependencies here, for eg:
  #install_requires=['foo>=3'],

  # Fill in these to make your Egg ready for upload to
  # PyPI
  author = 'jswaro',
  author_email = '',

  #summary = 'Just another Python package for the cheese shop',
  url = 'http://bitbucket.org/jswaro/pymonitor',
  license = '',
  long_description= 'Long description of the package',

  # could also include long_description, download_url, classifiers, etc.
  scripts = ['pyMonitor.py', 'Element.py', 'Interface.py', 'IpAddress.py',
		'Route.py', 'Screen.py', 'Utils.py', 'Walk.py'],
  packages = [],
  package_dir={},  
)
