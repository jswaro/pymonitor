class Element:
    def __init__(self):
        self.width = 0;
        self.height = 0;
        self.arr = [];

    def addString(self, value):
        self.width   = self.gt(self.width, len(value));
        self.height += 1;
        self.arr.append(value);

    def addElement(self, value):
        self.width   = self.gt(self.width, value.width);
        self.height += value.height;

        for entry in value.arr:
            self.arr.append(entry);

    def gt(self,x, y):
        if( x  > y ):
            return x
        return y;

    def toString(self):
        x = str(self.width) + " " + str(self.height) + "\n";
        for entry in self.arr:
            x += str(entry) + "\n";

        return x;

def getIPRouteElement(routes):
    e = Element();

    s = "Destination".ljust(18, " ") + "  " + "NextHop".ljust(18, " ");
    e.addString(s);
    for entry in routes:
        routeElement = routes[entry].returnElement();
        e.addElement(routeElement);

    return e;

def getInterfacesElement(interfaces):
    e = Element();

    s = "Interface".ljust(16, " ") + "A O " + "IP Address".rjust(18, " ");
    e.addString(s);

    for entry in interfaces:
        if( not interfaces[entry].hasIP() ):
            continue;

        interfaceElement = interfaces[entry].returnElement();

        e.addElement(interfaceElement);

    return e

def getRouterElement( walk ):
    e = Element();

    interfaceData = getInterfacesElement(walk["interfaces"])
    routeData = getIPRouteElement(walk["routes"]);

    e.addElement(interfaceData);
    e.addElement(routeData);

    return e;