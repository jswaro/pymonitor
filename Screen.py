SCREEN_NARROW_SMALL = 0;
SCREEN_NARROW_TALL = 1;
SCREEN_WIDE_SMALL = 2;
SCREEN_WIDE_TALL = 3;

import curses

class Monitor:
    def __init__(self, screen):
        self.stdscr = screen;
        self.x = 80;
        self.y = 24;
        self.screenElements = {};

    def addScreenElement(self, e, type):
        if( type not in self.screenElements ):
            self.screenElements[type] = [];
        if( e not in self.screenElements[type] ):
            self.screenElements[type].append(e);

    def updateScreen(self, e):
        self.screenElements = e;

        self.refresh();

    def drawUtilityBar(self):
        s = "Q - Quit 1,2,3,4 - ScreenSize ".ljust(self.x);

        self.stdscr.addstr(20,0,s);

    def redrawScreen(self):
        #stubbed
        self.updateScreen(self.screenElements);
        self.refresh();
        return;

    def resize(self, i):
        if( i == SCREEN_NARROW_SMALL or i == SCREEN_NARROW_TALL):
            self.x = 80;
        else:
            self.x = 132;
        if( i == SCREEN_NARROW_SMALL or i == SCREEN_WIDE_SMALL ):
            self.y = 24;
        else:
            self.y = 43;

        self.stdscr.resize(y,x);
        self.redrawScreen();

    def refresh(self):
        self.drawUtilityBar();
        self.stdscr.refresh();

