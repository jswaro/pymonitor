from Element import Element
import re

LAB_IP = re.compile("^132\.235\.201\.*");
TEN_DOT = re.compile("^10\.*\.*\.*");
LOCAL = re.compile("^127\.*\.*\.*");
ONENINETWO = re.compile("^192\.168\.*\.*");

IPFILTERS = [LAB_IP, TEN_DOT, LOCAL, ONENINETWO];

CIDR_TO_MASK= {"0" : "0.0.0.0",
               "1" : "128.0.0.0",
               "2" : "192.0.0.0",
               "3" : "224.0.0.0",
               "4" : "240.0.0.0",
               "5" : "248.0.0.0",
               "6" : "252.0.0.0",
               "7" : "254.0.0.0",
               "8" : "255.0.0.0",
               "9" : "255.128.0.0",
               "10": "255.192.0.0",
               "11": "255.224.0.0",
               "12": "255.240.0.0",
               "13": "255.248.0.0",
               "14": "255.252.0.0",
               "15": "255.254.0.0",
               "16": "255.255.0.0",
               "17": "255.255.128.0",
               "18": "255.255.192.0",
               "19": "255.255.224.0",
               "20": "255.255.240.0",
               "21": "255.255.248.0",
               "22": "255.255.252.0",
               "23": "255.255.254.0",
               "24": "255.255.255.0",
               "25": "255.255.255.128",
               "26": "255.255.255.192",
               "27": "255.255.255.224",
               "28": "255.255.255.240",
               "29": "255.255.255.248",
               "30": "255.255.255.252",
               "31": "255.255.255.254",
               "32": "255.255.255.255"}

MASK_TO_CIDR = {};

for each in CIDR_TO_MASK:
    MASK_TO_CIDR[CIDR_TO_MASK[each]] = each;
    
def isSafeAddr(addr):
    for each in IPFILTERS:
        if (each.search(addr) != None):
            return True;
    return False;

class IPEntry:
    def __init__(self):
        self.interfaceid = None;
        self.address = "";
        self.netmask = "";

    def getID(self):
        return self.interfaceid;
    
    def getAddress(self):
        return self.address;
    
    def getMask(self):
        return self.netmask;

    def setID(self,id):
        self.interfaceid = id;
    
    def setAddress(self, addr):
        self.address = addr;
        
    def setMask(self, mask):
        self.netmask = mask;
        
    def __repr__(self):
        return repr([self.interfaceid, self.address, self.netmask]);