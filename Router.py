from Walk import getWalk
from Route import Route
from SNMPFilters import *
from Interface import Interface, IGNORED_INTERFACES
from Address import IPEntry, isSafeAddr


def match(filter):
    return filter + "."

class Router:
    def __init__(self, address, name, snmpversion):
        self.ip = address;
        self.name = name;
        self.snmpversion = snmpversion;
        self.interfaces = {};
        self.addresses = {}
        self.routes = {};
        self.errors = [];
        
    
    def __updateRoutingTable(self):
        results = getWalk(self.ip, self.snmpversion, mibs["ip"] + ".21.1");
    
        for each in results:
            try:
                ip = ".".join(each.split("=")[0].split(".")[-4:]).strip();
                value = each.split("=")[1];
                value = value[value.find(":") + 1:].strip();
            except Exception as e:
                print each; 
                print e; 
                continue;
            
            if (ip not in self.routes):
                self.routes[ip] = Route();
                
            index = each.find(match(ipSubMibs["ipRouteDest"]));
            if (index != -1):
                self.routes[ip].setDest(value);
                continue;
        
            index = each.find(match(ipSubMibs["ipRouteIfIndex"]));
            if (index != -1):
                self.routes[ip].setInterfaceID(value);
                continue;
            
            index = each.find(match(ipSubMibs["ipRouteNextHop"]));
            if (index != -1):
                self.routes[ip].setNext(value);
                continue;
            
            index = each.find(match(ipSubMibs["ipRouteMask"]));
            if (index != -1):
                self.routes[ip].setMask(value);
                continue;
                
        return;
    
    def __updateInterfaces(self):
        
        results = getWalk(self.ip, self.snmpversion, mibs["SNMP MIB-2 Interfaces"]+".2");
    
        for each in results:
            try:
                id = ".".join(each.split("=")[0].split(".")[-1:]).strip();
                value = each.split("=")[1];
                value = value[value.find(":")+1:].strip()
            except Exception as e:
                print each;
                print e;
                continue;
            
            if (id not in self.interfaces):
                self.interfaces[id] = Interface();
                
            interface = self.interfaces[id];
            interface.setID(id);
                
            index = each.find(match(ifSubMibs["ifDescr"]));
            if (index != -1):
                interface.setDescription(value);
                continue;
        
            index = each.find(match(ifSubMibs["ifType"]));
            if (index != -1):
                interface.setType(value);
                continue;
            
            index = each.find(match(ifSubMibs["ifAdminStatus"]));
            if (index != -1):
                interface.setAdminStatus(value);
                continue;
            
            index = each.find(match(ifSubMibs["ifOperStatus"]));
            if (index != -1):
                interface.setOperStatus(value);
                continue;
            
            index = each.find(match(ifSubMibs["ifLastChange"]));
            if (index != -1):
                interface.setLastChange(value);
                continue;
            
                
        return;
    
    def __updateAddresses(self):
        results = getWalk(self.ip, self.snmpversion, ipAddressTbl);
    
        for each in results:
            try:
                id = ".".join(each.split("=")[0].split(".")[-4:]).strip();
                value = each.split("=")[1];
                value = value[value.find(":")+1:].strip()
            except Exception as e:
                print each;
                print e;
                continue;
            
            if (id not in self.addresses):
                self.addresses[id] = IPEntry();
                
            entry = self.addresses[id];
                
            index = each.find(match(ipAddressSubs["ipAdEntAddr"]));
            if (index != -1):
                entry.setAddress(value);
                continue;
        
            index = each.find(match(ipAddressSubs["ipAdEntIfIndex"]));
            if (index != -1):
                entry.setID(value);
                continue;
            
            index = each.find(match(ipAddressSubs["ipAdEntNetMask"]));
            if (index != -1):
                entry.setMask(value);
                continue;
                
        return;
    
    def __updateErrors(self):
        self.detectErrors([]);
    
    def update(self):
        self.__updateRoutingTable();
        self.__updateInterfaces();
        self.__updateAddresses();
        self.__updateErrors();
        return;
    
    
    
    def detectErrors(self, routers):
        self.errors = [];
        
        error = "no default route";
        if ("0.0.0.0" not in self.routes and error not in self.errors):
            self.errors.append(error);
            
        ''' interface errors '''
        for x in self.interfaces:
            interface = self.interfaces[x];
            desc = interface.getShortDesc()
            
            if (desc in IGNORED_INTERFACES):
                continue;
            
            error = "{0:s} conf !connected".format(desc);
            if (interface.getAdminStatus() == "U" and 
                interface.getOperStatus() == "D"):
                self.errors.append(error);
                
            error = "{0:s} !conf connected".format(desc);
            if (interface.getAdminStatus() == "D" and
                interface.getOperStatus() == "U"):
                self.errors.append(error); 
            
        ''' addressing errors '''
        for x in self.addresses:
            ip = self.addresses[x];
            if( isSafeAddr(ip.getAddress()) ):
                ''' look for strange subnets '''
                error = "{0:s} weird address: {1:s}".format(self.interfaces[ip.getID()].getShortDesc(),
                                                            ip.getAddress());
                self.errors.append(error);
            else:
                error = "{0:s} weird address: {1:s}".format(self.interfaces[ip.getID()].getShortDesc(),
                                                            ip.getAddress());
                self.errors.append(error);
                    
        
        ''' check for errors present on other routers w.r.t. to current'''
        for r in routers:
            if (r.name == self.name):
                continue;
            
            
    

    ''' refresh interfaces and route information '''
    def __refresh__(self):
        return;
    