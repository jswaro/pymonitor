import subprocess

def getWalk(ipAddress, snmpversion="3", filter=""):
    print str(filter)
    snmpExec = ["snmpwalk", "-c", "public", "-v", snmpversion , "-On", ipAddress, str(filter)];

    results = [];

    proc = subprocess.Popen(snmpExec, shell=False, stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT);

    for line in proc.stdout:
        data = line.rstrip();
        results.append(data);
    
    return results;



