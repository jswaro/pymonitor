def CIDR(mask):
    value = mask.split(".");
    cidr = 32;
    for x in range(3, -1, -1):
        current = int(value[x]);
        if( current == 255 ):
            break;
        elif( current == 254 ):
            cidr -= 1;
        elif( current == 252 ):
            cidr -= 2;
        elif( current == 248 ):
            cidr -= 3;
        elif( current == 240 ):
            cidr -= 4;
        elif( current == 224 ):
            cidr -= 5;
        elif( current == 192 ):
            cidr -= 6;
        elif( current == 128 ):
            cidr -= 7;
        else:
            cidr -= 8;

    return str(cidr);