from Element import Element
from Utils import CIDR

IGNORED_INTERFACES = ["Serial1/0", "Serial1/1", "Serial1/2", "Serial1/3"];

class Interface:
    def __init__(self):
        self.id = "";
        self.descr = "";
        self.adminStatus = "down";
        self.operStatus = "down";
        self.lastchange = ""
        self.iftype = ""

    def getShortDesc(self):
        if(self.descr.find("Ethernet") != -1):
            return "ETH" + self.descr[-3:];
        else:
            return self.descr;
        
    def getAdminStatus(self):
        if (self.adminStatus.find("down") != -1):
            return "D"
        return "U"
    
    def getOperStatus(self):
        if (self.operStatus.find("down") != -1):
            return "D"
        return "U"

    def setID(self,id):
        self.id = id;
        
    def setDescription(self, dsc):
        self.descr = dsc;
        
    def setAdminStatus(self, status):
        self.adminStatus = status;
    
    def setOperStatus(self, status):
        self.operStatus = status;
        
    def setLastChange(self, t):
        self.lastchange = t;
        
    def setType(self,t):
        self.iftype = t;

    def __repr__(self):
        return repr([self.id, self.getShortDesc(), self.getAdminStatus(),
            self.getOperStatus(), self.iftype, self.lastchange]);
