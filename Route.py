from Element import Element
from Utils import CIDR

class Route:
    def __init__(self):
        self.dst = "";
        self.mask = "";
        self.next = "";
        self.id = -1;

    def setDest(self, dest):
        self.dst = dest;
        
    def setMask(self, mask):
        self.mask = mask;
        
    def setNext(self, next):
        self.next = next;
        
    def setInterfaceID(self, id):
        self.id = id;

    def __repr__(self):
        return repr([self.dst, self.next, self.mask, self.id])

    def returnElement(self):
        ret = Element();

        x = (self.dst + "/" + CIDR(self.dst)).ljust(18," ") + "  " + self.next.ljust(18," ");

        ret.addString(x);

        return ret;

    def parseLine( self, field, entry ):
        if(field == "ipCidrRouteDest"):
            self.dst = entry[0].strip();
        elif(field == "ipCidrRouteMask"):
            self.mask = entry[0].strip();
        elif(field == "ipCidrRouteNextHop"):
            self.next = entry[0].strip();
        elif(field == "ipCidrRouteIfIndex"):
            self.ifIndex = int(entry[0].strip());
        elif(field == "ipCidrRouteStatus"):
            if( entry[0].strip().split("(")[0] == "active" ):
                self.active = True;
        return;

    def toValue(self):
        return [self.id, self.dst, self.mask, self.next, self.ifIndex, self.active];

    def toString(self):
        return "ID: " + self.id + "\nDst: " + self.dst + "\tMask: " + self.mask +\
            "\tNext: " + self.next + "\nUsing Interface ID " + repr(self.ifIndex);